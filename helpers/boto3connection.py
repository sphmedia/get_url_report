from asyncio.log import logger
import boto3


class Boto3connection:

    def get_connection(aws_region, service):

        try:
            dynamodb = boto3.resource(service, region_name=aws_region)

            return dynamodb
        except Exception as e:
            logger.info(e)

    def get_client_connection(aws_region, service):

        try:
            dynamodb = boto3.client(service, region_name=aws_region)

            return dynamodb
        except Exception as e:
            logger.info(e)
