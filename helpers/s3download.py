from asyncio.log import logger
from http import client
import boto3


class S3GetObject:

    def s3_get_object(self, connection, bucket, key):
        try:
            response = connection.get_object(Bucket=bucket, Key=key)

            content = response['Body']

            return content

        except Exception as e:
            logger.info(e)

    def s3_get_file(self, client, bucket, key):
        '''
        Task get object from s3 bucket based on the data format
        parameters
        ----------
        client: s3 client
        bucket: s3 bucket name
        key: file sub path
        file_type : json or csv
        '''

        responce = self.s3_get_object(
            client, bucket, key)

        return responce