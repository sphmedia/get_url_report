from asyncio.log import logger

import yaml


class ConfigReader:

    def read_config(file_path):
        ''' 
        Task read the config file and return values
        parameters
        ----------
        file_path: file path which is need to read
        '''
        with open(file_path) as stream:
            try:
                data = yaml.safe_load(stream)
                return data

            except yaml.YAMLError as e:
                logger.info(e)
