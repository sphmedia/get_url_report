'''
author uyanushka@gmail.com
'''
import calendar
from asyncio.log import logger
from datetime import datetime, timedelta
from xml.dom.minidom import Attr
from boto3.dynamodb.conditions import Key, Attr


class KeyCondition:

    def getdata_by_keycondition(self, current_time, previos_time, name, table, connection):
        ''' Takes in query data from dynamodb
            arguments
            -----------------------
            user_id: id of the user
            campain_id: campain id
            table; dynamodb table name
            connection: dynamodb client
        '''
        table = connection.Table(table)
        try:
            response = table.scan(
                FilterExpression=Attr("time").lte(current_time) & Attr("time").gte(previos_time) & Attr("name").eq(name)
            )

            return response['Items']

        except KeyError as e:
            logger.info(e)

    def orchestrate_data(self, df, items, dbconnection, db_table):
        '''
        Task orchestrate data read by csv and format to response
        parameters
        -----------------------
        campain_data: csv file name
        '''

        last_hour_date_time = datetime.now() - timedelta(hours = 1)
        last_hour_date_time = calendar.timegm(last_hour_date_time.timetuple())
        
        current_time = calendar.timegm(datetime.now().timetuple())

        for index, row in df.iterrows():
            # i = 0
            name = row["name"]
            
            item = self.getdata_by_keycondition(current_time, last_hour_date_time, name, db_table, dbconnection)
            items.append(item)
                

        return items
