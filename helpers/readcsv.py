import imp
import pandas as pd
import requests

from asyncio.log import logger


class Readcsv:

    def readcsv(file_path, chunck_size):

        try:
            chunck = pd.read_csv(file_path, chunksize=chunck_size)
            return chunck
        except FileNotFoundError as e:
            logger.info(e)

    def orchestrate_campain_data(df, items):
        '''
        Task orchestrate data read by csv and format to response
        parameters
        -----------------------
        campain_data: csv file name
        '''

        for index, row in df.iterrows():
            # i = 0
            name = row["name"]
            url = row["url"]

            try:
                r = requests.head(url)

                response = {"data": {}}

                response["data"] = {"name": name,
                                    "url": url, "status_code": r.status_code}

                items.append(response)

            except requests.exceptions.RequestException as e:
                response = {"data": {}}

                response["data"] = {"name": name,
                                    "url": url, "status_code": "404"}

                items.append(response)
                logger.info(e)

        return items
