import threading as thr
import pandas as pd
from crypt import methods
from distutils.log import debug
from flask import Flask, request, jsonify
from helpers.readcsv import Readcsv
from helpers.getdata import KeyCondition
from helpers.s3download import S3GetObject
from helpers.configreader import ConfigReader as cr
from werkzeug.utils import secure_filename
from helpers.boto3connection import Boto3connection
app = Flask(__name__)


@app.route('/health', methods=['GET'])
def health():
    return jsonify({'response': 'success'})


@app.route('/report', methods=['GET'])
def report():

    s3client = Boto3connection.get_client_connection(region, 's3')

    s3_operator = S3GetObject()

    url_info = s3_operator.s3_get_file(
        s3client, s3_bucket, s3_key)
    url_info = Readcsv.readcsv(url_info, chunk_size)

    dynamodb = Boto3connection.get_connection(region, 'dynamodb')

    threads = []
    items = []

    kc = KeyCondition()

    for rows in url_info:
        print(rows)
        df = pd.DataFrame(rows)

        df = df.reset_index()

        t = thr.Thread(target=kc.orchestrate_data,
                       args=(df, items, dynamodb, db_table))
        t.start()
        threads.append(t)

    for thread in threads:
        thread.join()

    return jsonify(items)


if __name__ == '__main__':
    config_data = cr.read_config("helpers/configs/config.yaml")
    chunk_size = config_data["chunck_size"]
    host = config_data["host"]
    service_port = config_data["service_port"]
    region = config_data["region"]
    s3_bucket = config_data["s3_bucket"]
    s3_key = config_data["s3_key"]
    db_table = config_data["db_table"]
    app.run(host=host, port=service_port, debug=True)
